var app = angular.module('main', ['ngRoute']);

	
	app.config(function ($routeProvider) {
	    $routeProvider

	        .when('/', {
	            templateUrl: 'home.html',
	            controller: 'mainController'
	        })

	       
	        .when('/allcds', {
	            templateUrl: 'allcds.html',
	            controller: 'allController'
	        })

	       
	        .when('/newcds', {
	            templateUrl: 'newcds.html',
	            controller: 'newController'
	        });
	});

	
	app.controller('mainController', function ($scope) {
	   
	    $scope.name = 'Record City';
	    $scope.headOffice = "Las Vegas, NV, USA";

	    $scope.getDetails = function () {
	        return [
                {title : 'Hacksaw Ridge', language : 'English', price : '$12'},
                {title : 'Batman Begins', language : 'English', price :'$10'},
                {title : 'Gladiator', language : 'English', price :'$12'},
                {title : 'The Dark Knight Rises', language : 'English', price :'$8'},
                {title : 'Wonder', language : 'English', price :'$10'} 
                ]
	    }
	});

	app.controller('allController', function ($scope) {
	      $scope.name = 'Record City';
	    $scope.headOffice = "Las Vegas, NV, USA";


	    $scope.getDetails = function () {
	        return [
	            {title : 'Hacksaw Ridge', language : 'English', price : '$12'},
                {title : 'Zootopia', language : 'English', price :'$8'},
                {title : 'John Wick', language : 'English', price : '$9'},
                {title : 'Batman Begins', language : 'English', price :'$10'},
                {title : 'Despicable Me', language : 'English', price :'$9'},
                {title : 'The Lord of the Rings', language : 'English', price :'$8'},
                {title : 'Gladiator', language : 'English', price :'$12'},
                {title : 'The Dark Knight Rises', language : 'English', price :'$8'},
                {title : 'Interstellar', language : 'English', price :'$8'},
                {title : 'Deadpool', language : 'English', price :'$10'},
                {title : 'Inside Out', language : 'English', price :'$8'},
                {title : 'Kung Fu Panda', language : 'English', price :'$11'},
                {title : 'The Fate of the Furious', language : 'English', price :'$9'},
                {title : 'Wonder', language : 'English', price :'$10'}
                ]
	    }
	});

	app.controller('newController', function ($scope) {
	      $scope.name = 'Record City';
	    $scope.headOffice = "Las Vegas, NV, USA";

	    $scope.getDetails = function () {
	        return [
	         {title : 'Hacksaw Ridge', language : 'English', price : '$12'},
             {title : 'Zootopia', language : 'English', price :'$8'},
             {title : 'John Wick', language : 'English', price : '$9'},
             {title : 'Kung Fu Panda', language : 'English', price :'$11'},
             {title : 'The Fate of the Furious', language : 'English', price :'$9'},
             {title : 'Wonder', language : 'English', price :'$10'}
	         ]
	    }
	});
